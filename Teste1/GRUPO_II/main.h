/* 
 * File:   main.h (GRUPO II)
 * Author: Daniel Torres <a17442|at|alunos.ipca.pt>
 * 
 * Created on 6 de Dezembro de 2018, 19:25
 */

#ifndef MAIN_H
#define MAIN_H

void mostraArray(int arrEntrada[], int sz);
void mostraArrayFloat(float arrEntrada[], int sz);

#endif /* MAIN_H */

