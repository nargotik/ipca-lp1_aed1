/*!
 * @header      funcoes.c (GRUPO II)
 * @abstract    Grupo II.
 * @discussion  Resolução do Grupo II.
 * @author      Daniel Torres <a17442|at|alunos.ipca.pt>
 * 
 * @ref         Crivo de Eratóstenes (Desempenho da funcao isPrimo)
 *                  https://pt.wikipedia.org/wiki/Crivo_de_Erat%C3%B3stenes
 * @version     1.0 6 de Dezembro de 2018
 */

#include "funcoes.h"

/**
 * Pergunta: Implemente uma função 
 * 
 * int isolaPrimos(int valores[],int primos[],int n) que, dado um array de n
 * valores numéricos, devolve o conjunto dos seus valores que sejam primos.

 * @param valores
 * @param primos
 * @param n
 * @return 
 */
int isolaPrimos(int valores[],int primos[],int n) {
    int qtyPrimos = 0;
    // Ciclo de i até n-1
    for (int i = 0; i < n; i++ ) {
        
        // Caso o valor seja primo ...
        if ( isPrimo(valores[i]) ) {
            primos[qtyPrimos] = valores[i];
            qtyPrimos++;
        }
    }
    return qtyPrimos;
}

/**
 * Pergunta: Implemente o procedimento 
 * void maioresM(float m[][4], float s[]) 
 * que, dada uma matriz 3x4 de valores reais, seja capaz de 
 * devolver o maior de cada uma das suas colunas. Esses valores
 * devem ser colocados no array s. Por exemplo:
 * 
 * @param m Matriz 3x4
 * @param s Matriz dos Maiores
 */
void maioresM(float m[][4], float s[])  {
    const int COLUNAS = 4;
    const int LINHAS  = 3;
    
    float auxmaior =0;
    // Ciclo da coluna de 1 até 4 colunas
    for ( int coluna = 0; coluna < COLUNAS; coluna++) {
        
        // O maior é o primeiro
        auxmaior = m[0][coluna];
        for (int linha = 1; linha < LINHAS; linha++) {
            
            // Verifica se o atual é maior que o auxiliar
            if ( m[linha][coluna] > auxmaior)
                auxmaior = m[linha][coluna];
            
        }

        // No final de correr as linhas da coluna já temos o maior valor
        // Logo atribuise esse valor a s
        s[coluna] = auxmaior;
        
    }
}


/*!
 * Função que verifica se um dado numero é primo
 * @param n Numero
 * @return boolean
 */
bool isPrimo(int n) {
    
    // O 1 e o 2 é primo
    if (n==2) 
        return true;
    if (n==1)
        return true;
    
    // Todos os pares não sao primos
    if (n%2 == 0)
        return false;
    // Proximos numeros primos
    if (n==3 || n==5 || n==7 || n==11 || n==13 || n== 17 || n==19)
        return true;
    // Verifica o resto da divisao por um deles possivel
    if (n%3==0 || n%5==0 || n%7==0 || n%11==0 || n%13==0 || n%17==0 || n%19==0 )
        return false;

    // Caso nao seja um caso destes itera
    // Para i=19 ate i<=sqrt(n)
    int tocheck = sqrt(n);//n / 2; // se n>n/2 entao n % n > 0
    for (int i = 19; i <= tocheck; i = i + 2) { // Os pares ja foram todos verificados
        // Verifica se é divisivel por i
        if (n % i ==0)
            return false;
    }
    return true;
}