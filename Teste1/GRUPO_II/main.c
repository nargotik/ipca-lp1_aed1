/*!
 * @header      main.c (GRUPO II)
 * @abstract    Grupo II.
 * @discussion  Resolução do Grupo II.
 * @author      Daniel Torres <a17442|at|alunos.ipca.pt>
 * @ref         Crivo de Eratóstenes
 *                  https://pt.wikipedia.org/wiki/Crivo_de_Erat%C3%B3stenes
 * @version     1.0 6 de Dezembro de 2018
 */


#include <stdio.h>
#include <stdlib.h>
#include "funcoes.h"
#include "main.h"

/*!
 * @function    main
 */
int main()
{
    // Exercicio 1 e 3
    int arr[5] = { 1,2,3,4,5 };
    int primos[5] = { 0,0,0,0,0};
    
    int qtyPrimos = 0;
    qtyPrimos = isolaPrimos(arr,primos,5);
    
    mostraArray(primos,qtyPrimos);
    
    
    // EOF Exercicio 1 e 3
    
    
    // Exercicio 2 e 3
    // Define a matriz apresentada no enunciado
    float m[3][4] = { { 4, 2, 3, 5 }, { 3 , 1 , 1 , 3 }, { 6 , 2 , 2 , 1 } };
    float s[4];
    
    // Calcula Maiores por coluna
    maioresM(m,s);
    puts("\n=======================================\n");
    puts("Array dos maiores por coluna:\n");
    mostraArrayFloat(s,4);
    // EOF Exercicio 2 e 3
    return (EXIT_SUCCESS);
}



/**
 * Função que mostra um array na consola
 * @param arrEntrada
 * @param sz
 */
void mostraArray(int arrEntrada[], int sz) {
    for (int i=0;i < sz; i++)
        printf("%d,",arrEntrada[i]);
}

/**
 * Função que mostra um array na consola
 * @param arrEntrada
 * @param sz
 */
void mostraArrayFloat(float arrEntrada[], int sz) {
    for (int i=0;i < sz; i++)
        printf("%f\t",arrEntrada[i]);
}