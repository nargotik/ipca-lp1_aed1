/* 
 * File:   funcoes.h (Grupo II)
 * Author: Daniel Torres <a17442|at|alunos.ipca.pt>
 * 
 * Created on 6 de Dezembro de 2018, 19:12
 */

#ifndef FUNCOES_H
#define FUNCOES_H

#include <stdbool.h>
#include <math.h>

bool isPrimo(int n);
void maioresM(float m[][4], float s[]);
int isolaPrimos(int valores[],int primos[],int n);


#endif /* FUNCOES_H */

