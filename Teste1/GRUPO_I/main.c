/*!
 * @header      main.c
 * @abstract    Resolução do grupo I
 * @discussion  Resolução do grupo I
 * @author      Daniel Torres <a17442|at|alunos.ipca.pt>
 * @version     1.0 6 de Dezembro de 2018
 */


#include <stdio.h>
#include <stdlib.h>
#include "main.h"
#include "funcoes.h"

/*!
 * @function    main
 * @abstract    main program function.
 */
int main()
{
    int valores[TAMANHO];
    
    ler_inteiros(valores);

    printf("Produtos dos valores Positivos: %d \n", 
            produtoValoresPos(valores, TAMANHO) );
    
    
    printf("Média dos valores Positivos: %d \n", 
            mediaValoresPos(valores, TAMANHO) );
    
    // Armazenar pares e quantidade
    int pares[TAMANHO], qtyPares=0;
    inicializaArrayInt(pares,TAMANHO,0);
    
    qtyPares = separaValoresPares(valores, pares, TAMANHO);
    printf("Quantidade de pares: %d \n", 
            qtyPares );
    if (qtyPares > 0) {
        puts("==== Numeros Pares: ");
        mostraArray(pares, qtyPares);
    } else {
        puts("==== Não há pares");
    }
    
    
    
    return (EXIT_SUCCESS);
}

/**
 * Função que mostra um array na consola
 * @param arrEntrada
 * @param sz
 */
void mostraArray(int arrEntrada[], int sz) {
    for (int i=0;i < sz; i++)
        printf("%d,",arrEntrada[i]);
}

/**
 * Função que lê TAMANHO valores e coloca no array de entrada
 * @param array de valores
 */
void ler_inteiros(int valores[]) {
    /*
     * Funcao a implementar em ciclos de leitura ...
     */
    int n=0;
    do {
        printf("Indique o valor %d de %d:", n+1,TAMANHO);
        valores[n] = ler_inteiro();
        n++;
    } while (( n < TAMANHO ));
}

/**
 * Função que lê um numero inteiro
 * @return numero inteiro
 */
int ler_inteiro() {
    int n;
    int read_result = -1;
    do {
        if (read_result != -1) printf("Valor Inválido, Repita:");
        read_result = scanf("%d",&n);
        getchar(); // Limpa \n do buffer stdin
    } while (read_result == 0);
    
    return n;
}
