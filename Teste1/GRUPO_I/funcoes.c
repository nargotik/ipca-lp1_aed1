/*!
 * @header      funcoes.c
 * @abstract    Funções pedidas no grupo I
 * @discussion  Resolução do grupo I
 * @author      Daniel Torres <a17442|at|alunos.ipca.pt>
 * @version     1.0 6 de Dezembro de 2018
 */
#include "funcoes.h"


/**
 * Devolve o produto dos numeros positivos de um array
 * @param arrEntrada Array de entrada
 * @param sz
 * @return 
 */
int produtoValoresPos(int arrEntrada[],int sz) {
    // Inicializa o produto a 1 (neutro da multiplicação)
    int produto = 1;
    for (int i=0;i < sz; i++) {
        // Se o valor for positivo efetua o produto
        if (arrEntrada[i] > 0 ) {
            produto *= arrEntrada[i];
        }
    }
    return produto;
}


/**
 * Devolve a média dos valores positivos contidos num dado array
 * @param arrEntrada
 * @param sz
 * @return 
 */
int mediaValoresPos(int arrEntrada[],int sz) {
    // Inicializa o somatorio dos valores
    int somatorio = 1;
    // Quantidade de valores positivos
    int positivos = 0;
    
    for (int i=0;i < sz; i++) {
        // Se o valor for positivo efetua o produto
        if (arrEntrada[i] > 0 ) {
            positivos++;
            somatorio += arrEntrada[i];
        }
    }
    
    // Para não efetuar a divisão por zero
    return ( (positivos > 0) ? (somatorio / positivos) : 0 );
}

/**
 * 
 * @param arrEntrada
 * @param pares array de saida dos valores pares
 * @param sz
 * @return numero de valores pares
 */
int separaValoresPares(int arrEntrada[],int pares[],int sz) {
    // Inicializa o numero de pares
    int qtPares = 0;
    
    for (int i=0;i < sz; i++) {
        // Se o valor for positivo efetua o produto
        if ( par(arrEntrada[i]) == TRUE ) {
            // At
            pares[qtPares] = arrEntrada[i];
            qtPares++;
        }
    }
    
    // devolve a quantidade de pares
    return ( qtPares );
}

/**
 * Verifica se um dado valor é par
 * @param valor
 * @return true se par
 */
bool par(int valor) {
    return ( ((valor % 2) == 0) );
}

/**
 * Inicializa um array com um dado valor em todos os seus elementos
 * @param arrEntrada
 * @param sz
 * @param valor
 */
void inicializaArrayInt(int arrEntrada[],int sz,int valor) {
    for (int i=0;i < sz; i++)
        arrEntrada[i] = valor;

}