/* 
 * File:   main.h (Grupo I)
 * Author: Daniel Torres <a17442|at|alunos.ipca.pt>
 * 
 * Created on 6 de Dezembro de 2018, 18:35
 */

#ifndef MAIN_H
#define MAIN_H

#define TAMANHO 3

void ler_inteiros(int valores[]);
int ler_inteiro();
void mostraArray(int arrEntrada[], int sz);

#endif /* MAIN_H */

