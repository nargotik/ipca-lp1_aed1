/* 
 * File:   funcoes.h (GRUPO I)
 * Author: Daniel Torres <a17442|at|alunos.ipca.pt>
 * 
 * Created on 6 de Dezembro de 2018, 18:41
 */

#ifndef FUNCOES_H
#define FUNCOES_H


typedef int bool;
#define TRUE 1
#define FALSE 0


int produtoValoresPos(int array[],int sz);
int mediaValoresPos(int array[],int sz);
int separaValoresPares(int array[],int pares[],int sz);
bool par(int valor);
void inicializaArrayInt(int arrEntrada[],int sz,int valor);




#endif /* FUNCOES_H */

