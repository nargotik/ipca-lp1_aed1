/*!
 * @header      main.c
 * @abstract    Grupo III
 * @discussion  Grupo III.
 * @author      Daniel Torres <a17442|at|alunos.ipca.pt>
 * @version     1.0 6 de Dezembro de 2018
 */


#include <stdio.h>
#include "main.h"

/*!
 * @function    main

 */
int main()
{
    
    int teste = ler_inteiro();
    printf("Resultado da funcao: %d",xpto(teste));
}

/**
 * A função o valor inserido seguido da sua inversa
 * Ex. 1234 -> devolve 12344321
 * 
 * @param x valor a c
 * @return 
 */
int xpto(int x)
{
    int aux = 0, ini = x, cont = 1;
    while (x > 0) {
        aux = aux * 10 + (x % 10); // O x%10 serve para ir buscar o ultimo digito
                                   // Multiplica-se o ultimo digito anterior por 10
                                   // Soma-se o ultimo digito para inverter
        x = x / 10;                // Pega-se no resto inteiro da divisão para o proximo ciclo
        cont*=10;
    }
    
    // Aux é o valor da inversa
    // Ini é o valor inicial
    // Cont = 1*10*10*....N até à divisao inteira de x dar 0 
    
    
    // Exemplo Calculos
    //
    // Inicio
    // (Ciclo 1)
    // 1234 % 10 = 4
    // 0*10+4 = 4
    // 1234/10 = 123
    //
    // (Ciclo 2)
    // 123 % 10 = 3
    // 4 * 10 + 3 = 43
    // 123/10 = 12
    //
    // (Ciclo 3)
    // 12 % 10 = 2
    // 43 * 10 + 2 = 432
    // 12 / 10 = 1
    //
    // (Ciclo 4)
    // 1 % 10 = 1
    // 432 * 10 + 1 = 1234
    // 1 / 10 = 0 (0 = fim ciclo)
    //
    // cont = 1234 = 10*10*10*10 = 10000
    // 10000 * 1234 = 1234 0000
    // 1234 000 + 4321 = 12344321
    return (ini*cont + aux);
}



/**
 * Função que lê um numero inteiro
 * @return numero inteiro
 */
int ler_inteiro() {
    int n;
    puts("Indique um valor:");
    int read_result = -1;
    do {
        if (read_result != -1) printf("Valor Inválido, Repita:");
        read_result = scanf("%d",&n);
        getchar(); // Limpa \n do buffer stdin
    } while (read_result == 0);
    
    return n;
}