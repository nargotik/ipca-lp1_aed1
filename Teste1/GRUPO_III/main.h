/* 
 * File:   main.h
 * Author: Daniel Torres <a17442|at|alunos.ipca.pt>
 * 
 * Created on 6 de Dezembro de 2018, 20:14
 */

#ifndef MAIN_H
#define MAIN_H

int ler_inteiro();
int xpto(int x);

#endif /* MAIN_H */

