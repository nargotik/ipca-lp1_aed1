/*!
 * @header      main.c
 * @abstract    Resolução do grupo I.
 * @author      Daniel Torres <a17442|at|alunos.ipca.pt>
 * @link        Daniel Torres GIT <https://github.com/nargotik>
 * @version     1.0 17 de Dezembro de 2018
 */


#include <stdio.h>
#include <stdlib.h>

unsigned int funcao_1(unsigned int min, unsigned int max);
int ler_inteiro(char* msg);
unsigned int funcao_2(int paragem);

int main()
{
    int min = ler_inteiro("Insira o valor minimo");
    int max = ler_inteiro("Insira o valor maximo");
    printf("%u",funcao_1(min,max));
    
    puts("\n\nInsira valores e termine com 0\n");
    unsigned int negativos = funcao_2(0);
    printf("Valores negativos inseridos %u",negativos);
    return (EXIT_SUCCESS);
}

/**
 * Função do grupo I questão 1
 * @param min
 * @param max
 * @return Numero de pares encontrados na sequencia
 */
unsigned int funcao_1(unsigned int min, unsigned int max) {
    int pares=0;
    // Verifica se o min é maior ou igual ao max
    if (max<=min) return 0;
    for (unsigned int i = min; i <= max; i++) {
        printf("%u-",i);
        if ((i%2) == 0)
            pares++;
    }
    return pares;
}

/**
 * Função do grupo I questão 2
 * @param paragem
 * @return numero de negativos
 */
unsigned int funcao_2(int paragem) {
    /*
     * Funcao a implementar em ciclos de leitura ...
     */
    int n=-1, negativos=0;
    do {
        n = ler_inteiro("Indique um numero inteiro:");
        if (n<0) negativos++;
    } while (( n != paragem ));
    return negativos;
}

/**
 * Função macro
 * Função que lê um numero inteiro
 * @return numero inteiro
 */
int ler_inteiro(char* msg) {
    int n;
    int read_result = -1;
    printf("%s:",msg);
    do {
        if (read_result != -1) printf("Valor Inválido, Repita:");
        read_result = scanf("%d",&n);
        getchar(); // Limpa \n do buffer stdin
    } while (read_result == 0);
    
    return n;
}