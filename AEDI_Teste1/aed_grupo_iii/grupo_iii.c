/*!
 * @header      main.c
 * @abstract    Resolução do grupo III.
 * @author      Daniel Torres <a17442|at|alunos.ipca.pt>
 * @link        Daniel Torres GIT <https://github.com/nargotik>
 * @ref         some reference
 * @version     1.0 17 de Dezembro de 2018
 */


#include <stdio.h>
#include <stdlib.h>



typedef struct Funcionario {
    int codigo;
    char* nome;
    int idade;
    int cod_profissao;
} Funcionario; 

char* lerString(char* msg);
int lerInteiro(char* msg);
Funcionario LerFuncionario();
int existeIndice(Funcionario arr[], float cod, int total);
float MediaIdades(Funcionario arr[],int total, int cod_profissao);
int remFuncionario(Funcionario arr[],int total, int cod_funcionario);

#define MAXFUNCIONARIOS 50

int main()
{
    int total = 0, aux2=0;
    Funcionario arrFuncionarios[MAXFUNCIONARIOS];
    
    Funcionario aux;
    
    
    int registos = lerInteiro("Insira o numero de registos a inserir");
    
    for (int i=0; i < registos ;i++) {
        printf("==========================\n"
                "Insira o registo %d\n\n",i+1);
        
        aux = LerFuncionario();
        // Verifica se é um update
        aux2 = existeIndice(arrFuncionarios,aux.codigo,total);
        if (aux2 == -1) {
            // Insere
            arrFuncionarios[total] = aux;
            total++;
        } else {
            // Update
            arrFuncionarios[aux2] = aux;
        }
        
    }
    
    printf("Media idades profissao 100 %f",
            MediaIdades(arrFuncionarios, total,100 )
            );
           
    printf("\nTotal antes de remover o cod 100 %d\n", total);
    total = remFuncionario(arrFuncionarios, total, 100);
    printf("\nTotal apos de remover o cod 100 %d\n", total);
    return (EXIT_SUCCESS);
}

/**
 * Le um funcionario
 * @return um funcionario
 */
Funcionario LerFuncionario() {
    Funcionario aux;
    aux.codigo          = lerInteiro("Insira o Codigo");
    aux.nome            = lerString("Insira o Nome");
    aux.idade           = lerInteiro("Insira a Idade");
    aux.cod_profissao   = lerInteiro("Insira o codigo Profissao");
    return aux;
}


/**
 * Remove um funcionario
 * @param arr
 * @param total
 * @param cod_funcionario
 * @return novo total
 */
int remFuncionario(Funcionario arr[],int total, int cod_funcionario) {
    int aux = existeIndice(arr,cod_funcionario,total);
    if (aux == -1) {
        // Funcionario nao existe
        return total;
    } else {
        // funcionario encontrado passa a ser o ultimo e reduz o total
        // Nao apaga o ultimo pois o apontador(total) passa para total-1
        arr[aux] = arr[total];
        return(total-1);
    }
    
}

/**
 * Media de idades para uma profissao
 * @param arr
 * @param total
 * @param cod_profissao
 * @return media de idades
 */
float MediaIdades(Funcionario arr[],int total, int cod_profissao) {
    float soma = 0;
    int total_registos=0;
    for (int i = 0; i < total; i++) {
        if (arr[i].cod_profissao == cod_profissao) {
            soma += arr[i].idade;
            total_registos++;
        }
    }
    return(total_registos > 0 ? (soma / total_registos) : 0);
}

/**
 * Função que lê um numero inteiro
 * @return numero inteiro
 */
int lerInteiro(char* msg) {
    int n;
    int read_result = -1;
    printf("%s:",msg);
    do {
        if (read_result != -1) printf("Valor Inválido, Repita:");
        read_result = scanf("%d",&n);
        getchar(); // Limpa \n do buffer stdin
    } while (read_result == 0);
    
    return n;
}

/**
 * Função que lê um numero inteiro
 * @return numero inteiro
 */
char* lerString(char* msg) {
    char* n;
    int read_result = -1;
    printf("%s:",msg);
    do {
        if (read_result != -1) printf("Valor Inválido, Repita:");
        read_result = scanf("%s",&n);
        getchar(); // Limpa \n do buffer stdin
    } while (read_result == 0);
    
    return n;
}

/**
 * Verifica se um codigo existe no array de funcionarios
 * @param arr
 * @param cod
 * @param total
 * @return -1 se nao existir, indice se existir
 */
int existeIndice(Funcionario arr[], float cod, int total) {
    for (int i = 0; i < total; i++) {
        if (arr[i].codigo == cod) return i;
    }
    return -1;
}