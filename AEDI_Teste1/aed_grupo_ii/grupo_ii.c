/*!
 * @header      main.c
 * @abstract    Resolução do grupo II.
 * @author      Daniel Torres <a17442|at|alunos.ipca.pt>
 * @link        Daniel Torres GIT <https://github.com/nargotik>
 * @version     1.0 17 de Dezembro de 2018
 */


#include <stdio.h>
#include <stdlib.h>

#define MAXPRODUTOS 50
#define COLS 2

typedef float tipoarray;

int existeIndice(tipoarray arr[MAXPRODUTOS][COLS], float cod, int total);
int lerInteiro(char* msg);
int lerRegisto(tipoarray arr[MAXPRODUTOS][COLS], int total);
float lerFloat(char* msg);
int leDados(int numero,tipoarray arr[MAXPRODUTOS][COLS], int total);
float Media(tipoarray arr[MAXPRODUTOS][COLS], int total);
float ValorPagar(tipoarray arr[MAXPRODUTOS][COLS], int total,float cod, int qt);

/*!
 * @function    main
 * @abstract    main program function.
 * @param       argc    ....
 * @param       argv     ....
 * @result      An integer result of this function.
 */
int main()
{
    int total = -1;
    tipoarray produtos[MAXPRODUTOS][COLS];
    
    int registos = lerInteiro("Insira o numero de registos a inserir");
    total = leDados(registos, produtos, total);

    printf("Media %f", Media(produtos, total));
    printf("Valor a pagar do cod 101 %f", ValorPagar(produtos, total, 101, 10));
    return (EXIT_SUCCESS);
}

/**
 * Media 
 * @param arr
 * @param total
 * @return 
 */
float Media(tipoarray arr[MAXPRODUTOS][COLS], int total) {
    float soma=0;
    // O apontador aponta para o registo total-1
    int total_registos = total + 1;
    for (int i = 0; i <= total; i++) {
        soma += arr[i][1];
    }
    return (total_registos>0) ? (soma/total_registos) : 0;
}

/**
 * Valor a pagar para uma determinada qty
 * @param arr
 * @param total
 * @param cod
 * @param qt
 * @return 
 */
float ValorPagar(tipoarray arr[MAXPRODUTOS][COLS], int total,float cod, int qt) {
    int indice = existeIndice(arr, cod, total);
    if (indice == -1) {
        return 0;
    } else {
        return(arr[indice][1] * qt);
    }

}

/**
 * Le dados
 * @param numero
 * @param arr
 * @param total
 * @return 
 */
int leDados(int numero,tipoarray arr[MAXPRODUTOS][COLS], int total) {
    
    
    for (int i=0; i < numero ;i++) {
        printf("==========================\n"
                "Insira o registo %d\n\n",i+1);
        
        total = lerRegisto(arr, total);
    }
    return total;
}

/**
 * Le registo
 * @param arr
 * @param total
 * @return 
 */
int lerRegisto(tipoarray arr[MAXPRODUTOS][COLS], int total) {
    int novo_cod = lerInteiro("Insira o codigo");
    
    int aux = existeIndice(arr, novo_cod, total);
    /*if (aux!=-1) {
        printf("ENCONTRADO %d",aux);
    }*/
    int indice = ( aux == -1) ? total+1 : aux;
    
    //printf("%s %d total:%d",(!(indice > total)) ? "UPDATE" : "INSERT", indice,total);
    
    arr[indice][0] = novo_cod;
    arr[indice][1] = lerInteiro("Insira o preco (int)");
    
    // Caso o indice seja um novo retorna o novo senao mantem o tamanho
    return (indice > total) ? indice : total;

    
}

/**
 * Verifica se existe um indice
 * @param arr
 * @param cod
 * @param total
 * @return 
 */
int existeIndice(tipoarray arr[MAXPRODUTOS][COLS], float cod, int total) {

    for (int i = 0; i <= total; i++) {
        if (arr[i][0] == cod) return i;
    }
    return -1;
}

/**
 * Função que lê um numero inteiro
 * @return numero inteiro
 */
int lerInteiro(char* msg) {
    int n;
    int read_result = -1;
    printf("%s:",msg);
    do {
        if (read_result != -1) printf("Valor Inválido, Repita:");
        read_result = scanf("%d",&n);
        getchar(); // Limpa \n do buffer stdin
    } while (read_result == 0);
    
    return n;
}

/**
 * Função que lê um numero inteiro
 * @return numero float
 */
float lerFloat(char* msg) {
    float n;
    int read_result = -1;
    printf("%s:",msg);
    do {
        if (read_result != -1) printf("Valor Inválido, Repita:");
        read_result = scanf("%f",&n);
        getchar(); // Limpa \n do buffer stdin
    } while (read_result == 0);
    
    return n;
}